#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266mDNS.h>

#ifndef STASSID
//#define STASSID "2.4G-Vectra-WiFi-49B0A8"
//#define STAPSK  "p4bgwr6oh09jzb03"
#define STASSID "iPhone (Grzegorz)"
#define STAPSK  "dupadupa123"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

const int alarm = 0;
const int led = 2;

static int ALARM_STATE = 0;
static int previousAlarmState = 0;

static String deviceId = "0000000001";
static String movementDetected = "false";
static String deviceStatus = "ON";

String getResponseJSON() {
  String response = "{";
  response += "\"deviceId\": \"" + deviceId + "\",";
  response += "\"status\": \"" + deviceStatus + "\",";
  response += "\"deviceIp\": \"" + WiFi.localIP().toString() + "\",";
  response += "\"movementDetected\": \"" + movementDetected + "\"";
  response += "}";

  return response;
}

void handleRoot() {
  Serial.println("--------------------------");
  Serial.println("Message handled...");
  String responseString = getResponseJSON();
  server.send(200, "application/json", responseString);
  Serial.println("Response sending complete!");
}

void sendNotification() {
Serial.println("--------------------------");
Serial.println("Message handled...");
 if(WiFi.status()== WL_CONNECTED){                                                  //Check WiFi connection status
   HTTPClient http;                                                                 //Declare object of class HTTPClient
   
   http.begin("http://us-central1-wpam-grswi.cloudfunctions.net/app/events");      //Specify request destination
   //http.begin("http://wpameiti.free.beeceptor.com/alarm");
   String requestString = getResponseJSON();
   http.addHeader("Content-Type", "application/json");
   http.addHeader("Content-Length", String(requestString.length()));
   //String messageString = getResponseJSON(deviceId, deviceStatus, movementDetected);
   int httpCode = http.POST(requestString);                                         //Send the request
   String payload = http.getString();                                               //Get the response payload
   Serial.println(httpCode);                                                        //Print HTTP return code
   Serial.println(payload);                                                         //Print request response payload
   if(httpCode == 200) { 
       Serial.println("Movement data sent successfully!");  
   } else {
       Serial.println("Movement data message sending error!");  

   }
   http.end();    
 }else{
    Serial.println("Error in WiFi connection. Cannot send alarm message!");   
 }
 Serial.println("Response sending complete!");
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  pinMode(led, OUTPUT);
  pinMode(alarm, INPUT);
  digitalWrite(led, 0); // ZERO TO ŚWIECENIE SIĘ !
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.on("/state", handleRoot);
  server.onNotFound(handleNotFound);
  
  server.begin();
  Serial.println("HTTP server started");
  //Say hello :)
  sendNotification();
}

void loop(void) {
  server.handleClient();
  MDNS.update();
  
  ALARM_STATE = digitalRead(alarm);
  if(ALARM_STATE != previousAlarmState) {
    if(ALARM_STATE == 1) {
      Serial.println("Checking movement...");
      movementDetected = "true";
      deviceStatus = "ALARM";
      digitalWrite(led, 0);
      sendNotification();
      Serial.println("MOVEMENT DETECTED !!!");
    } else {
      movementDetected = "false";
      deviceStatus = "OK";
      digitalWrite(led, 1);
    }
  }
  previousAlarmState = ALARM_STATE;
}

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>

#ifndef STASSID
//#define STASSID "2.4G-Vectra-WiFi-49B0A8"
//#define STAPSK  "p4bgwr6oh09jzb03"
#define STASSID "iPhone (Grzegorz)"
#define STAPSK  "dupadupa123"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

DynamicJsonDocument doc(200);

const int switcher = 0;
const int led = 2;

static String deviceId = "0000000002";
static String deviceStatus = "ON";
static int deviceState = 0;


String getResponseJSON() {
  String response = "{";
  response += "\"deviceId\": \"" + deviceId + "\",";
  response += "\"status\": \"" + deviceStatus + "\",";
  response += "\"deviceIp\": \"" + WiFi.localIP().toString() + "\",";
  response += "\"state\": " + String(deviceState);
  response += "}";

  return response;
}

void handleRoot() {
  Serial.println("--------------------------");
  Serial.println("Message handled...");
  server.send(200, "text/plain", "hello from esp8266!");
  Serial.println("Response sending complete!");
}

void switchHandler() {
  Serial.println("--------------------------");
  Serial.println("Message handled...");
  Serial.println(server.arg("plain"));
  Serial.println((server.method() == HTTP_GET) ? "GET" : "POST");

  if(server.method() != HTTP_GET) {
    DeserializationError error = deserializeJson(doc, server.arg("plain"));
    if(error) {
      Serial.println("deserializeJson() failed. Received message: ");
      Serial.println(server.arg("plain"));
      server.send(500, "application/json", "{\"\message\" : \"Message Deserializing Error\"}");
      return;
    }
    
    int status = doc["state"];
  
    Serial.println("Received status: ");
    Serial.println(status);
  
    if(status == 0) {
      digitalWrite(led, 0);
      digitalWrite(switcher, 0);
      deviceState = digitalRead(switcher);

    } else {
      digitalWrite(led, 1);
      digitalWrite(switcher, 1);
      deviceState = digitalRead(switcher);
    }
  } 

  server.send(200, "application/json", getResponseJSON());
  Serial.println("Response sending complete!");
}

void handleNotFound() {
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

void sendNotification() {
 if(WiFi.status()== WL_CONNECTED){                                                  //Check WiFi connection status
   HTTPClient http;                                                                 //Declare object of class HTTPClient
   
   http.begin("http://us-central1-wpam-grswi.cloudfunctions.net/app/events");      //Specify request destination
   //http.begin("http://wpameiti.free.beeceptor.com/alarm");
   String requestString = getResponseJSON();
   http.addHeader("Content-Type", "application/json");
   http.addHeader("Content-Length", String(requestString.length()));
   //String messageString = getResponseJSON(deviceId, deviceStatus, movementDetected);
   int httpCode = http.POST(requestString);                                         //Send the request
   String payload = http.getString();                                               //Get the response payload
   Serial.println(httpCode);                                                        //Print HTTP return code
   Serial.println(payload);                                                         //Print request response payload
   if(httpCode == 200) { 
       Serial.println("Switch message sent successfully!");  
   } else {
       Serial.println("Switch message sending error!");  

   }
   http.end();    
 }else{
    Serial.println("Error in WiFi connection. Cannot send switch message!");   
 }
}

void setup(void) {
  pinMode(led, OUTPUT);
  pinMode(switcher, OUTPUT);
  digitalWrite(led, 0);
  digitalWrite(switcher, 0);
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/switch", switchHandler);
  
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
  
  sendNotification();
}

void loop(void) {
  delay(10);
  server.handleClient();
  MDNS.update();
}

#include "DHT.h"

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>


#ifndef STASSID
//#define STASSID "2.4G-Vectra-WiFi-49B0A8"
//#define STAPSK  "p4bgwr6oh09jzb03"
#define STASSID "iPhone (Grzegorz)"
#define STAPSK  "dupadupa123"
#endif

DHT dht(2, DHT11);

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

DynamicJsonDocument doc(200);

static String deviceId = "0000000003";
static String deviceStatus = "ON";
static float humidity = -1.0;
static float temperature = -1.0;
static String requestString = "";

static int httpCode = 201;
static String payload = "";

HTTPClient http;                                                                 //Declare object of class HTTPClient


String getResponseJSON() {
  if(isnan(humidity)) humidity = -1.0;
  if(isnan(temperature)) temperature = -1.0;
  
  requestString = "{";
  requestString += "\"deviceId\": \"" + deviceId + "\",";
  requestString += "\"status\": \"" + deviceStatus + "\",";
  requestString += "\"humidity\": " + String(humidity) + ",";
  requestString += "\"deviceIp\": \"" + WiFi.localIP().toString() + "\",";
  requestString += "\"temperature\": " + String(temperature);
  requestString += "}";

  return requestString;
}

void handleRoot() {
  Serial.println("--------------------------");
  Serial.println("Message handled...");
  server.send(200, "text/plain", "hello from esp8266!");
  Serial.println("Response sending complete!");
}

void parametersHandler() {
  Serial.println("--------------------------");
  Serial.println("Message handled...");
  Serial.println(server.arg("plain"));
  Serial.println((server.method() == HTTP_GET) ? "GET" : "POST");


  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  Serial.print("Temperature = ");
  Serial.println(temperature);
  Serial.print("Humidity = ");
  Serial.println(humidity);

  server.send(200, "application/json", getResponseJSON());
  Serial.println("Response sending complete!");
  
}

void handleNotFound() {
  requestString = "File Not Found\n\n";
  requestString += "URI: ";
  requestString += server.uri();
  requestString += "\nMethod: ";
  requestString += (server.method() == HTTP_GET) ? "GET" : "POST";
  requestString += "\nArguments: ";
  requestString += server.args();
  requestString += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    requestString += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", requestString);
}

void sendNotification() {
 if(WiFi.status()== WL_CONNECTED){                                                  //Check WiFi connection status   
   http.begin("http://us-central1-wpam-grswi.cloudfunctions.net/app/events");      //Specify request destination
   //http.begin("http://wpameiti.free.beeceptor.com/alarm");
   requestString = getResponseJSON();
   http.addHeader("Content-Type", "application/json");
   http.addHeader("Content-Length", String(requestString.length()));
   //String messageString = getResponseJSON(deviceId, deviceStatus, movementDetected);
   httpCode = http.POST(requestString);                                         //Send the request
   payload = http.getString();                                               //Get the response payload
   Serial.println(httpCode);                                                        //Print HTTP return code
   Serial.println(payload);                                                         //Print request response payload
   if(httpCode == 200) { 
       Serial.println("Parameters message sent successfully!");  
   } else {
       Serial.println("Parameters message sending error!");  

   }
   http.end();    
 }else{
    Serial.println("Error in WiFi connection. Cannot send switch message!");   
 }
}

void setup(void) {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/parameters", parametersHandler);
  
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");

  dht.begin();
  delay(500);
  
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  Serial.print("Temperature = ");
  Serial.println(temperature);
  Serial.print("Humidity = ");
  Serial.println(humidity);
  Serial.println("DHT11 INIT COMPLETE");
  
  sendNotification();
}

void loop(void) {
  delay(10);
  server.handleClient();
  MDNS.update();
}

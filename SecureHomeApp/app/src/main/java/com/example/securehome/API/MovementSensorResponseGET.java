package com.example.securehome.API;

import com.example.securehome.Util;

public class MovementSensorResponseGET extends ResponseWrapper {
    String movementDetected;
    String deviceId;
    Util.DeviceStatus status;

    public String getMovementDetected() {
        return movementDetected;
    }

    public void setMovementDetected(String movementDetected) {
        this.movementDetected = movementDetected;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Util.DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(Util.DeviceStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MovementSensorResponseGET{" +
                "movementDetected='" + movementDetected + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", status=" + status +
                '}';
    }
}
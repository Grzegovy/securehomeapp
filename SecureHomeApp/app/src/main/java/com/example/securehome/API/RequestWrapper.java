package com.example.securehome.API;

public class RequestWrapper {
    String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    @Override
    public String toString() {
        return "RequestWrapper{" +
                "deviceId='" + deviceId + '\'' +
                '}';
    }
}

package com.example.securehome.API;

public class SwitchRequestPOST extends RequestWrapper {
    Integer state;

    @Override
    public String toString() {
        return "SwitchRequestPOST{" +
                "state=" + state +
                '}';
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}

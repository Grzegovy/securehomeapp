package com.example.securehome.API;

import com.example.securehome.Util;

public class SwitchResponseGET extends ResponseWrapper {
    Integer state;
    String deviceId;
    Util.DeviceStatus status;

    @Override
    public String toString() {
        return "SwitchResponseGET{" +
                "state=" + state +
                ", deviceId='" + deviceId + '\'' +
                ", status=" + status +
                '}';
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Util.DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(Util.DeviceStatus status) {
        this.status = status;
    }
}
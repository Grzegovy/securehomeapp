package com.example.securehome.API;

public class TemperatureSensorRequestGET extends RequestWrapper {
    Float temperature;
    Float humidity;

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Float getHumidity() {
        return humidity;
    }

    public void setHumidity(Float humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "TemperatureSensorRequestGET{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", deviceId='" + deviceId + '\'' +
                '}';
    }
}
package com.example.securehome.API;

import com.example.securehome.Util;

public class TemperatureSensorResponseGET extends ResponseWrapper {
    Float temperature;
    Float humidity;
    String deviceId;
    Util.DeviceStatus status;

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Float getHumidity() {
        return humidity;
    }

    public void setHumidity(Float humidity) {
        this.humidity = humidity;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Util.DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(Util.DeviceStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TemperatureSensorResponseGET{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", deviceId='" + deviceId + '\'' +
                ", status=" + status +
                '}';
    }
}
package com.example.securehome;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

public class ContactsListAdapter extends ArrayAdapter<M_Contact> {
    private final Context context;
    private final List<M_Contact> contacts;
    private final String roomName;
    private ContactsListAdapter adapter;

    public ContactsListAdapter(Context context, List<M_Contact> contacts, String roomName) {
        super(context, R.layout.activity_display_devices, contacts);
        this.context = context;
        this.contacts = contacts;
        this.roomName = roomName;
        this.adapter  = null;
    }

    public void bindAdapter(ContactsListAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public M_Contact getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_display_contact_list_element, parent, false);
        TextView contactName = (TextView) rowView.findViewById(R.id.contactNameLB);
        TextView contactPhone = (TextView) rowView.findViewById(R.id.contactPhoneLB);
        Button removeContactBtn = (Button) rowView.findViewById(R.id.removeContactBTN);

        final M_Contact contact = this.contacts.get(position);
        contactName.setText(contact.getName());
        contactPhone.setText(contact.getNumber());

        removeContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Room room = RuntimeStorage.roomMap.get(roomName);
                room.removeContact(contact.getName());
                convertView.refreshDrawableState();
                Toast.makeText(context, "Contact deleted!", Toast.LENGTH_SHORT).show();
                remove(getItem(position));
                adapter.notifyDataSetChanged();
            }
        });

        return rowView;
    }
}
package com.example.securehome;

import android.support.annotation.NonNull;

import com.example.securehome.DevicesDataTypes.DT_MovementSensorData;
import com.example.securehome.DevicesDataTypes.DT_SwitchData;
import com.example.securehome.DevicesDataTypes.DT_TemperatureSensor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class DataLoader {

    public static Map<String, Room> getRooms() {
        System.out.println("LOADING ROOMS...");
        RuntimeStorage.loadingData.setLoadingData(true);

        // Get all rooms
        final Map<String, Room> roomMap = new HashMap<String, Room>();

        DatabaseConnector.roomsCollectionHandler.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
                                Room room = mapper.convertValue(document.getData(), Room.class);
                                roomMap.put(room.getName(), room);
                            }
                            RuntimeStorage.roomMap = roomMap;
                        } else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            System.out.println("SOME ERROR DB");
                        }
                        RuntimeStorage.loadingData.setLoadingData(false);
                    }
                });
        return roomMap;
    }

    public static Map<String, M_Device> getDevices() {
        RuntimeStorage.loadingData.setLoadingData(true);
        System.out.println("LOADING DEVICES...");
        // Get all rooms
        final Map<String, M_Device> devicesMap = new HashMap<String, M_Device>();

        DatabaseConnector.devicesCollectionHandler.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
                                M_Device device = mapper.convertValue(document.getData(), M_Device.class);
                                devicesMap.put(device.getDevideId(), device);

                                //Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                            //RuntimeStorage.roomMap = roomMap;
                        } else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            System.out.println("SOME ERROR DB");
                        }
                        RuntimeStorage.loadingData.setLoadingData(false);

                        // Init listener after rooms loaded
                        DatabaseConnector.monitorEventsChange();
                    }
                });
        return  devicesMap;
    }

    public static Map<String, M_Device> getUnusedDevices() {
        RuntimeStorage.loadingData.setLoadingData(true);

        System.out.println("GET UNUSED DEVICES...");
        final Map<String, M_Device> devices = new HashMap<>();

        DatabaseConnector.mFirestore.collection("devices").whereEqualTo("used", false).get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
                            M_Device device = mapper.convertValue(document.getData(), M_Device.class);
                            devices.put(device.getDevideId(), device);
                            System.out.println("FILTERED DATA..." + document.getData());
                        }
                    } else {
                        System.out.println("SOME ERROR DB");
                    }
                    RuntimeStorage.loadingData.setLoadingData(false);
                }
            });

        return devices;
    }

    public static NavigableMap<String, M_EventLog> getDeviceEvents(final String deviceId) {
        RuntimeStorage.loadingData.setLoadingData(true);

        System.out.println("GET EVENTS...");
        final TreeMap<String, M_EventLog> events = new TreeMap<>();
        DatabaseConnector.mFirestore.collection("events").whereEqualTo("deviceId", deviceId).orderBy("timestamp", Query.Direction.DESCENDING).limit(30).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper

                                Util.DeviceType type = RuntimeStorage.devicesMap.get(deviceId).getType();

                                switch(type) {
                                    case MOVEMENT_SENSOR: {
                                        DT_MovementSensorData event = mapper.convertValue(document.getData(), DT_MovementSensorData.class);
                                        events.put(event.getDateTime(), event);
                                        break;
                                    }
                                    case SWITCH: {
                                        DT_SwitchData event = mapper.convertValue(document.getData(), DT_SwitchData.class);
                                        events.put(event.getDateTime(), event);
                                        break;
                                    }
                                    case TEMPERATURE_SENSOR: {
                                        DT_TemperatureSensor event = mapper.convertValue(document.getData(), DT_TemperatureSensor.class);
                                        events.put(event.getDateTime(), event);
                                        break;
                                    }
                                    default: {
                                        System.out.println("Not supported device type...");
                                        break;
                                    }
                                }

                            }
                        } else {
                            System.out.println("SOME ERROR DB");
                        }

                        RuntimeStorage.loadingData.setLoadingData(false);
                    }
                });
        return events.descendingMap();
    }

    public static void refreshDeviceStatus(final M_Device device) {
        final String deviceId = device.getDevideId();
        DatabaseConnector.mFirestore.collection("events").whereEqualTo("deviceId", deviceId).orderBy("timestamp", Query.Direction.DESCENDING).limit(30).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper

                        Util.DeviceType type = RuntimeStorage.devicesMap.get(deviceId).getType();

                        switch(type) {
                            case MOVEMENT_SENSOR: {
                                DT_MovementSensorData event = mapper.convertValue(document.getData(), DT_MovementSensorData.class);
                                if (event.getStatus() == Util.DeviceStatus.ALARM && !event.getDeactivated()) {
                                    device.setStatus(Util.DeviceStatus.ALARM);
                                    return;
                                }
                                break;
                            }
                            case SWITCH: {
                                DT_SwitchData event = mapper.convertValue(document.getData(), DT_SwitchData.class);
                                if (event.getStatus() == Util.DeviceStatus.ALARM && !event.getDeactivated()) {
                                    device.setStatus(Util.DeviceStatus.ALARM);
                                    return;
                                }
                                break;
                            }
                            case TEMPERATURE_SENSOR: {
                                DT_TemperatureSensor event = mapper.convertValue(document.getData(), DT_TemperatureSensor.class);
                                if(event.getStatus() == Util.DeviceStatus.ALARM && !event.getDeactivated()) {
                                    device.setStatus(Util.DeviceStatus.ALARM);
                                    return;
                                }
                                break;
                            }
                            default: {
                                System.out.println("Not supported device type...");
                                break;
                            }
                        }
                    }
                } else {
                    System.out.println("SOME ERROR DB");
                }
            }
        });
    }
}

package com.example.securehome;

import android.app.Notification;
import android.util.ArraySet;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static android.content.ContentValues.TAG;

public class DatabaseConnector {
    public static FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    public static DatabaseReference eventsReference = firebaseDatabase.getReference("events");
    public static DatabaseReference devicesReference = firebaseDatabase.getReference("devices");

    public static FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    public static CollectionReference roomsCollectionHandler = mFirestore.collection("rooms");
    public static CollectionReference devicesCollectionHandler = mFirestore.collection("devices");
    public static CollectionReference eventsCollectionHandler = mFirestore.collection("events");


    public static void monitorEventsChange() {
        eventsCollectionHandler.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("TAG", "listen:error", e);
                    return;
                }

                Set<String> devicesToRefesh = new ArraySet<>();
                for (DocumentChange dc : snapshots.getDocumentChanges()) {
                    Map<String, Object> tempObject;
                    String tempDeviceId;
                    switch (dc.getType()) {
                        case ADDED:
                            tempObject = dc.getDocument().getData();
                            String deviceId = (String)tempObject.get("deviceId");
                            devicesToRefesh.add(deviceId);
                            break;
                        case MODIFIED:
                            tempObject = dc.getDocument().getData();
                            tempDeviceId = (String)tempObject.get("deviceId");
                            devicesToRefesh.add(tempDeviceId);
                            break;
                        case REMOVED:
                            break;
                    }

                }
                refreshDevicesStatus(devicesToRefesh);
            }
        });
    }

    public static void refreshDevicesStatus(Set<String> devicesIds) {
        for(String deviceId : devicesIds) {
            M_Device device = RuntimeStorage.devicesMap.get(deviceId);
            DataLoader.refreshDeviceStatus(device);
        }
    }
//
//    public static void initEventsListener() {
//        final DocumentReference docRef = mFirestore.collection("events").document("SF");
//        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot snapshot,
//                                @Nullable FirebaseFirestoreException e) {
//                if (e != null) {
//                    Log.w(TAG, "Listen failed.", e);
//                    return;
//                }
//
//                if (snapshot != null && snapshot.exists()) {
//                    Log.d(TAG, "Current data: " + snapshot.getData());
//                } else {
//                    Log.d(TAG, "Current data: null");
//                }
//            }
//        });
//    }

    public static void initListenersEventListener() {
        eventsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                List<Event> eventsToProces = new ArrayList<>();
                for(DataSnapshot ds : dataSnapshot.getChildren() ) {
                    Event event = ds.getValue(Event.class);
                    if(!event.processed) eventsToProces.add(event);
                }
                System.out.println("Events to process: " + eventsToProces);
                if(!eventsToProces.isEmpty()) {
                    // DO STH WITH EVENTS
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

    }}


    // Events
package com.example.securehome.DevicesDataTypes;

import com.example.securehome.M_EventLog;

public class DT_MovementSensorData extends M_EventLog {
    String movementDetected;

    public String getMovementDetected() {
        return movementDetected;
    }

    public void setMovementDetected(String movementDetected) {
        this.movementDetected = movementDetected;
    }

    @Override
    public String toString() {
        return "DT_MovementSensorData{" +
                "movementDetected='" + movementDetected + '\'' +
                '}';
    }
}

package com.example.securehome.DevicesDataTypes;

import com.example.securehome.M_EventLog;

public class DT_SwitchData extends M_EventLog {
    Boolean state;

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }
}

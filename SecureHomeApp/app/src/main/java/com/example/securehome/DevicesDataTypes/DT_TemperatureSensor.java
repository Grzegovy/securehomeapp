package com.example.securehome.DevicesDataTypes;

import com.example.securehome.M_EventLog;

public class DT_TemperatureSensor extends M_EventLog {
    Float temperature;
    Float humidity;


    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Float getHumidity() {
        return humidity;
    }

    public void setHumidity(Float humidity) {
        this.humidity = humidity;
    }
}


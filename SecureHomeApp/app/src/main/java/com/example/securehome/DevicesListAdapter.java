package com.example.securehome;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class DevicesListAdapter extends ArrayAdapter<M_Device> {
    private final Context context;
    private final List<M_Device> devices;

    public DevicesListAdapter(Context context, List<M_Device> devices) {
        super(context, R.layout.activity_display_devices, devices);
        this.context = context;
        this.devices = devices;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_display_devices_list_element, parent, false);
        TextView deviceId = (TextView) rowView.findViewById(R.id.deviceIdLB);
        TextView deviceName = (TextView) rowView.findViewById(R.id.deviceNameLB);
        TextView deviceStatus = (TextView) rowView.findViewById(R.id.deviceStatusLB);

        M_Device device = this.devices.get(position);
        if(device.getStatus() == Util.DeviceStatus.ALARM) {
            rowView.setBackgroundColor(Color.rgb(255,145,145));
        }

        deviceId.setText(device.getDevideId());
        deviceName.setText(device.getName());
        deviceStatus.setText(Util.getStatusNameByStatus(device.getStatus()));

        return rowView;
    }
}
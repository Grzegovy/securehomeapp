package com.example.securehome.DevicesSendRequest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.securehome.API.RequestWrapper;
import com.example.securehome.API.ResponseWrapper;
import com.example.securehome.API.SwitchRequestPOST;
import com.example.securehome.API.SwitchResponseGET;
import com.example.securehome.API.TemperatureSensorResponseGET;
import com.example.securehome.DatabaseConnector;
import com.example.securehome.DevicesDataTypes.DT_SwitchData;
import com.example.securehome.DevicesDataTypes.DT_TemperatureSensor;
import com.example.securehome.L_DisplayDevice;
import com.example.securehome.L_RoomDetails;
import com.example.securehome.M_Device;
import com.example.securehome.R;
import com.example.securehome.RuntimeStorage;
import com.example.securehome.Util;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DSR_Switch extends Activity {

    private Spinner statusChooser;
    private String deviceId;
    private String roomName;
    private M_Device device;

    TextView deviceId_LB;
    TextView deviceName_LB;
    TextView deviceEndpointGet_LB;
    TextView deviceEndpointPost_LB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_dsr_switch);
        initComponents();
    }

    public void initComponents() {
        initSpinner();
        Intent intent = getIntent();
        // Get room by name
        if (intent != null) {
            deviceId = intent.getStringExtra("deviceId");
            roomName = intent.getStringExtra("roomName");
        }
        device = RuntimeStorage.devicesMap.get(deviceId);
        initForm();
    }

    public void initForm() {
        deviceId_LB = (TextView) findViewById(R.id.deviceIdLB);
        deviceId_LB.setText(device.getDevideId());

        deviceName_LB = (TextView) findViewById(R.id.deviceNameLB);
        deviceName_LB.setText(device.getName());

        deviceEndpointPost_LB = (TextView) findViewById(R.id.endpointPostLB);
        deviceEndpointPost_LB.setText(getPostRequestEndpoint());

        deviceEndpointGet_LB = (TextView) findViewById(R.id.endpointGetLB);
        deviceEndpointGet_LB.setText(getGetRequestEndpoint());
    }

    public void initSpinner() {
        List<String> switchStatuses = new ArrayList<String>();
        switchStatuses.add("Set to OFF");
        switchStatuses.add("Set to ON");

        statusChooser = (Spinner) findViewById(R.id.switchValuesSpinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, switchStatuses);
        statusChooser.setAdapter(dataAdapter);

    }

    public void backToDevice(View view) {
        Intent intent = new Intent(DSR_Switch.this, L_DisplayDevice.class);
        intent.putExtra("deviceId", deviceId);
        intent.putExtra("roomName", roomName);
        startActivity(intent);
    }

    public String getPostRequestEndpoint() {
        String response = "http://" + device.getIpAddress();
        response += (device.getPort() != null && !device.getPort().equals("-")) ? ":" + device.getPort() : "" ;
        response += device.getPostDataEndpont();
        return response;
    }

    public String getGetRequestEndpoint() {
        String response = "http://" + device.getIpAddress();
        response += (device.getPort() != null && !device.getPort().equals("-")) ? ":" + device.getPort() : "" ;
        response += device.getGetDataEndpoint();
        return response;
    }

    public RequestWrapper getRequestPost() {
        String selectedStatus = statusChooser.getSelectedItem().toString();
        Util.DeviceStatus status = (selectedStatus.equals("Set to ON") ? Util.DeviceStatus.ON : Util.DeviceStatus.OFF);
        SwitchRequestPOST srw = new SwitchRequestPOST();
        srw.setDeviceId(device.getDevideId());
        System.out.println(status.ordinal());
        if(status.ordinal() == 0) {
            srw.setState(1);
        } else {
            srw.setState(0);
        }

        return srw;
    }


    public void sendDataToDevice(View view) {
        try {
            new SendPostRequestAsync(getPostRequestEndpoint()).execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void getDevicedata(View view) {
        try {
            new SendGetRequestAsync(getGetRequestEndpoint()).execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private class SendPostRequestAsync extends AsyncTask<String, Void, String> {
        AlertDialog alertDialog;

        String requestEndpoint;

        Integer statusCode = 500;
        String statusMessage = "";
        String message = "";

        public SendPostRequestAsync(String requestEndpoint) {
            this.requestEndpoint = requestEndpoint;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                ObjectMapper Obj = new ObjectMapper();
                String requestBodyString = Obj.writeValueAsString(getRequestPost());
                ResponseWrapper rw = sendPostRequest(requestEndpoint, requestBodyString);
                this.statusCode = rw.getStatusCode();
                this.statusMessage = rw.getStatusMessage();
                this.message = rw.getMessage();

                if(statusCode != null && statusCode == 200) {
                    String selectedStatus = statusChooser.getSelectedItem().toString();
                    device.setStatus((selectedStatus.equals("Set to ON") ? Util.DeviceStatus.ON : Util.DeviceStatus.OFF));
                    ObjectMapper obj = new ObjectMapper();

                    SwitchResponseGET response = obj.readValue(this.message, SwitchResponseGET.class);

                    this.message = String.format("Switch status is: %s.", (response.getState() == 0) ? "SWITCH ENABLED" : "SWITCH DISABLED");

                    DT_SwitchData event = new DT_SwitchData();
                    event.setState((response.getState() == 1) ? true : false);

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    Long dateTimeMs = date.getTime();

                    event.setDateTime(dateFormat.format(date));
                    event.setDeviceId(deviceId);
                    event.setDeactivated(true);
                    event.setStatus((response.getState() == 1) ? Util.DeviceStatus.ENABLED :  Util.DeviceStatus.DISABLED);
                    event.setTimestamp(dateTimeMs);

                    DatabaseConnector.eventsCollectionHandler.document(String.valueOf(dateTimeMs)).set(event);
                    device.setStatus((response.getState() == 1) ? Util.DeviceStatus.ENABLED :  Util.DeviceStatus.DISABLED);
                } else {
                    this.statusCode = 504;
                    this.message = "Device not connected to wifi network.";
                    this.statusMessage = "Connection timeout";
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                this.message = e.getMessage();
                e.printStackTrace();
            }

            return null;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = new AlertDialog.Builder(DSR_Switch.this).create();
            alertDialog.setTitle("Processing request...");
            alertDialog.setMessage("Please wait.");
            alertDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialog.dismiss();
            alertDialog = new AlertDialog.Builder(DSR_Switch.this).create();
            if(this.statusMessage.isEmpty()) {
                alertDialog.setTitle(String.format("Processing finished [%d].", this.statusCode));
            } else {
                alertDialog.setTitle(String.format("Processing finished: %s.", this.statusMessage));
            }
            alertDialog.setIcon(R.drawable.common_full_open_on_phone);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(this.message);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            alertDialog.show();
        }
    }

    private class SendGetRequestAsync extends AsyncTask<String, Void, String> {
        AlertDialog alertDialog;

        String requestEndpoint;

        Integer statusCode = 500;
        String statusMessage = "";
        String message = "";

        public SendGetRequestAsync(String requestEndpoint) {
            this.requestEndpoint = requestEndpoint;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                ResponseWrapper rw = sendGetRequest(requestEndpoint);
                this.statusCode = rw.getStatusCode();
                this.statusMessage = rw.getStatusMessage();
                this.message = rw.getMessage();

                if (statusCode != null && statusCode == 200) {
                    ObjectMapper obj = new ObjectMapper();
                    SwitchResponseGET response = obj.readValue(this.message, SwitchResponseGET.class);
                    System.out.println("SWITCH RESPONSE OK ! Status: " + response.getState());

                    this.message = String.format("Switch status is: %s.", (response.getState() == 0) ? "SWITCH ENABLED" : "SWITCH DISABLED");

                    DT_SwitchData event = new DT_SwitchData();
                    event.setState((response.getState() == 1) ? true : false);

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    Long dateTimeMs = date.getTime();

                    event.setDateTime(dateFormat.format(date));
                    event.setDeviceId(deviceId);
                    event.setDeactivated(true);
                    event.setStatus((response.getState() == 1) ? Util.DeviceStatus.ENABLED :  Util.DeviceStatus.DISABLED);
                    event.setTimestamp(dateTimeMs);

                    DatabaseConnector.eventsCollectionHandler.document(String.valueOf(dateTimeMs)).set(event);
                    device.setStatus((response.getState() == 1) ? Util.DeviceStatus.ENABLED :  Util.DeviceStatus.DISABLED);
                } else {
                    this.statusCode = 504;
                    this.message = "Device not connected to wifi network.";
                    this.statusMessage = "Connection timeout";
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                this.message = e.getMessage();
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = new AlertDialog.Builder(DSR_Switch.this).create();
            alertDialog.setTitle("Processing request...");
            alertDialog.setMessage("Please wait.");
            alertDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialog.dismiss();
            alertDialog = new AlertDialog.Builder(DSR_Switch.this).create();
            if(this.statusMessage.isEmpty()) {
                alertDialog.setTitle(String.format("Processing finished [%d].", this.statusCode));
            } else {
                alertDialog.setTitle(String.format("Processing finished: %s.", this.statusMessage));
            }
            alertDialog.setIcon(R.drawable.common_full_open_on_phone);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(this.message);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            alertDialog.show();
        }
    }


    public ResponseWrapper sendGetRequest(String url) throws IOException {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        URL urlObj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
        conn.setConnectTimeout(15000);

        try {
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            responseWrapper.setStatusCode(conn.getResponseCode());
            responseWrapper.setStatusMessage(conn.getResponseMessage());
            responseWrapper.setMessage(result.toString());
        } catch (IOException e) {
            System.out.println("EXCEPTION: Status Code: " + conn.getResponseCode());
            responseWrapper.setStatusCode(500);
            responseWrapper.setMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            return responseWrapper;
        }
    }

    public static ResponseWrapper sendPostRequest(String url, String requestStringBody) throws IOException {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        URL urlObj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Accept-Charset", "UTF-8");
        conn.setRequestProperty("Content-Type", "application/json");

        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);

        conn.connect();

        String paramsString = requestStringBody;

        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(paramsString);
        wr.flush();
        wr.close();

        try {
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            responseWrapper.setStatusCode(conn.getResponseCode());
            responseWrapper.setStatusMessage(conn.getResponseMessage());
            responseWrapper.setMessage(result.toString());
        } catch (IOException e) {
            System.out.println("EXCEPTION: Status Code: " + conn.getResponseCode());
            responseWrapper.setStatusCode(500);
            responseWrapper.setMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            return responseWrapper;
        }
    }
}


package com.example.securehome.DevicesSendRequest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.securehome.API.RequestWrapper;
import com.example.securehome.API.ResponseWrapper;
import com.example.securehome.API.TemperatureSensorRequestGET;
import com.example.securehome.API.TemperatureSensorResponseGET;
import com.example.securehome.DatabaseConnector;
import com.example.securehome.DevicesDataTypes.DT_TemperatureSensor;
import com.example.securehome.L_DisplayDevice;
import com.example.securehome.M_Device;
import com.example.securehome.R;
import com.example.securehome.RuntimeStorage;
import com.example.securehome.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.ServerValue;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DSR_TemperatureSensor extends Activity {

    private Spinner statusChooser;
    private String deviceId;
    private M_Device device;
    private String roomName;

    TextView deviceId_LB;
    TextView deviceName_LB;
    TextView deviceStatus_LB;
    TextView deviceEndpoint_LB;
    TextView deviceState_LB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_dsr_temperature_sensor);
        initComponents();
    }

    public void initComponents() {
        Intent intent = getIntent();
        // Get room by name
        if (intent != null) {
            deviceId = intent.getStringExtra("deviceId");
            roomName = intent.getStringExtra("roomName");
        }
        device = RuntimeStorage.devicesMap.get(deviceId);
        initForm();
    }

    public void initForm() {
        deviceId_LB = (TextView) findViewById(R.id.deviceIdLB);
        deviceId_LB.setText(device.getDevideId());

        deviceName_LB = (TextView) findViewById(R.id.deviceNameLB);
        deviceName_LB.setText(device.getName());

        deviceEndpoint_LB = (TextView) findViewById(R.id.endpointLB);
        deviceEndpoint_LB.setText(getGetRequestEndpoint());


    }

    public String getGetRequestEndpoint() {
        String response = "http://" + device.getIpAddress();
        response += (device.getPort() != null && !device.getPort().equals("-")) ? ":" + device.getPort() : "" ;
        response += device.getGetDataEndpoint();
        return response;
    }


    public void backToDevice(View view) {
        Intent intent = new Intent(DSR_TemperatureSensor.this, L_DisplayDevice.class);
        intent.putExtra("deviceId", deviceId);
        intent.putExtra("roomName", roomName);
        startActivity(intent);
    }

    private class SendRequestAsync extends AsyncTask<String, Void, String> {
        AlertDialog alertDialog;

        String requestEndpoint;

        Integer statusCode = 500;
        String statusMessage = "";
        String message = "";



        public SendRequestAsync(String requestEndpoint) {
            this.requestEndpoint = requestEndpoint;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                //ResponseWrapper rw = sendRequest("http://192.168.0.31/alarm");
                ResponseWrapper rw = sendGetRequest(requestEndpoint);
                this.statusCode = rw.getStatusCode();
                this.statusMessage = rw.getStatusMessage();
                this.message = rw.getMessage();

                if(statusCode != null && statusCode == 200) {
                    ObjectMapper obj = new ObjectMapper();
                    TemperatureSensorResponseGET response = obj.readValue(this.message, TemperatureSensorResponseGET.class);

                    this.message = String.format("Temperature: %.2f°C \n Humidity: %.2f%%",response.getTemperature(),response.getHumidity());

                    DT_TemperatureSensor event = new DT_TemperatureSensor();
                    event.setHumidity(response.getHumidity());
                    event.setTemperature(response.getTemperature());

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    Long dateTimeMs = date.getTime();

                    event.setDateTime(dateFormat.format(date));
                    event.setDeviceId(deviceId);
                    event.setDeactivated(true);
                    event.setStatus(Util.DeviceStatus.ON);
                    event.setTimestamp(dateTimeMs);

                    DatabaseConnector.eventsCollectionHandler.document(String.valueOf(dateTimeMs)).set(event);
                } else {
                    this.statusCode = 504;
                    this.message = "Device not connected to wifi network.";
                    this.statusMessage = "Connection timeout";
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                this.message = e.getMessage();
                e.printStackTrace();
            }

            return null;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = new AlertDialog.Builder(DSR_TemperatureSensor.this).create();
            alertDialog.setTitle("Processing request...");
            alertDialog.setMessage("Please wait.");
            alertDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialog.dismiss();
            alertDialog = new AlertDialog.Builder(DSR_TemperatureSensor.this).create();
            if(this.statusMessage.isEmpty()) {
                alertDialog.setTitle(String.format("Processing finished [%d].", this.statusCode));
            } else {
                alertDialog.setTitle(String.format("Processing finished: %s.", this.statusMessage));
            }
            alertDialog.setIcon(R.drawable.common_full_open_on_phone);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(this.message);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            alertDialog.show();
        }
    }


    public void getData(View view) {
        try {
            new SendRequestAsync(getGetRequestEndpoint()).execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public ResponseWrapper sendGetRequest(String url) throws IOException {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        URL urlObj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

        try {
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            responseWrapper.setStatusCode(conn.getResponseCode());
            responseWrapper.setStatusMessage(conn.getResponseMessage());
            responseWrapper.setMessage(result.toString());
        } catch (IOException e) {
            System.out.println("EXCEPTION: Status Code: " + conn.getResponseCode());
            responseWrapper.setStatusCode(500);
            responseWrapper.setMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            return responseWrapper;
        }
    }
}


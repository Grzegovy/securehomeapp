package com.example.securehome;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Event {
    public String id;
    public String deviceId;
    public String timestamp;
    public Boolean processed;
    public Util.DeviceType deviceType;
    public Object deviceStatusData;
    public Map<String, M_Device> devices;

    public Event() {
        this.devices = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public Util.DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Util.DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public Object getDeviceStatusData() {
        return deviceStatusData;
    }

    public void setDeviceStatusData(Object deviceStatusData) {
        this.deviceStatusData = deviceStatusData;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", processed=" + processed +
                ", deviceType=" + deviceType +
                ", deviceStatusData=" + deviceStatusData +
                '}';
    }
}

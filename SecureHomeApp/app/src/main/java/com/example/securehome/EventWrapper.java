package com.example.securehome;

public class EventWrapper {
    public String id;
    public Event event;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "EventWrapper{" +
                "id='" + id + '\'' +
                ", event=" + event +
                '}';
    }
}

package com.example.securehome;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.CircularBorderDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.securehome.DevicesDataTypes.DT_MovementSensorData;
import com.example.securehome.DevicesDataTypes.DT_SwitchData;
import com.example.securehome.DevicesDataTypes.DT_TemperatureSensor;

import java.util.List;


public class EventsListAdapter extends ArrayAdapter<M_EventLog> {
    private final Context context;
    private final List<M_EventLog> events;

    public EventsListAdapter(Context context, List<M_EventLog> events) {
        super(context, R.layout.activity_display_device, events);
        this.context = context;
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_display_event_list_element, parent, false);

        TextView datetime = (TextView) rowView.findViewById(R.id.datetimeLB);
        TextView status = (TextView) rowView.findViewById(R.id.statusLB);

        M_EventLog event = this.events.get(position);
        if(!event.getDeactivated() && event.getStatus() == Util.DeviceStatus.ALARM) {
            rowView.setBackgroundColor(Color.rgb(255,145,145));
        }

        datetime.setText(event.getDateTime());
        status.setText(Util.getStatusNameByStatus(event.getStatus()));

        prepareParametersView(event,rowView);
        return rowView;
    }

    public void prepareParametersView(M_EventLog event, View view) {
        if(event instanceof DT_MovementSensorData) {
            TableLayout tableLayout = (TableLayout) view.findViewById(R.id.eventTableLayout);
            tableLayout.addView(getTableRow("Movement Detected", String.valueOf(((DT_MovementSensorData) event).getMovementDetected())));
        } else if(event instanceof DT_TemperatureSensor) {
            TableLayout tableLayout = (TableLayout) view.findViewById(R.id.eventTableLayout);
            tableLayout.addView(getTableRow("Temperature: ", String.valueOf(((DT_TemperatureSensor) event).getTemperature())));
            tableLayout.addView(getTableRow("Humidity: ", String.valueOf(((DT_TemperatureSensor) event).getHumidity())));
        }else if(event instanceof DT_SwitchData) {
            TableLayout tableLayout = (TableLayout) view.findViewById(R.id.eventTableLayout);
            tableLayout.addView(getTableRow("Is active:  ", String.valueOf(((DT_SwitchData) event).getState())));
        }
    }

    public TableRow getTableRow(String paramName, String paramValue) {
        TableRow row = new TableRow(context);

        LinearLayout linearLayout = new LinearLayout(context);
            LinearLayout.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
            layoutParams.weight = 3;
            linearLayout.setLayoutParams(layoutParams);

            TextView tw = new TextView(context);
                LinearLayout.LayoutParams twParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                twParams.weight = 1;
                tw.setLayoutParams(twParams);
                tw.setText(paramName);
                tw.setGravity(Gravity.LEFT);
                linearLayout.addView(tw);

            TextView tw2 = new TextView(context);
                LinearLayout.LayoutParams tw2Params = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                tw2Params.weight = 1;
                tw2.setLayoutParams(tw2Params);
                tw2.setText(paramValue);
                tw2.setGravity(Gravity.RIGHT);
                linearLayout.addView(tw2);

        row.addView(linearLayout);

        return row;
    }
}
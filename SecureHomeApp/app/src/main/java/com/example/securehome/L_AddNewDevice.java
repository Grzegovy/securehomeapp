package com.example.securehome;

import android.app.Activity;
import android.content.Intent;
import android.os.storage.StorageManager;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class L_AddNewDevice extends Activity implements AdapterView.OnItemSelectedListener {

    M_Device device;
    Boolean editMode;

    EditText deviceNameTF;
    EditText deviceIdTF;
    Spinner selectDeviceTypeSP;
    EditText ipAddressTF;
    EditText portNF;
    EditText getEndpointTF;
    EditText postEndpointTF;
    CheckBox isAlarmCB;
    Button removeBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_device);
        setSpinnerValues();
        initVariables();

        Intent intent = getIntent();
        if(intent != null) {
            String devideId = intent.getStringExtra("deviceId");
            if (devideId != null) {
                device = RuntimeStorage.devicesMap.get(devideId);
                editMode = true;
                if(device.getUsed()) removeBT.setEnabled(false);
                loadFormData();
            } else {
                device = new M_Device();
                editMode = false;
                removeBT.setEnabled(false);
            }

        }
    }

    public void initVariables() {
        deviceIdTF =  (EditText) findViewById(R.id.deviceSerialNumberTF);
        deviceNameTF = (EditText) findViewById(R.id.deviceNameTF);
        selectDeviceTypeSP = (Spinner) findViewById(R.id.selectDeviceTypeSP);
        ipAddressTF = (EditText) findViewById(R.id.ipAddressTF);
        portNF = (EditText) findViewById(R.id.portNF);
        getEndpointTF = (EditText) findViewById(R.id.getEndpointTF);
        postEndpointTF = (EditText) findViewById(R.id.postEndpointTF);
        isAlarmCB = (CheckBox) findViewById(R.id.isAlarmCB);
        removeBT = (Button) findViewById(R.id.removeBT);
    }

    public void saveNewDevice(View view) {
        if(device == null) {
            device = new M_Device();

            device.setStatus(Util.DeviceStatus.OFF);
        }

        String deviceName = deviceNameTF.getText().toString();
        if(deviceName.isEmpty()) deviceName = "(unnamed)";
        device.setName(deviceName);

        String deviceId = deviceIdTF.getText().toString();
        if(deviceId.isEmpty()) deviceId = "(unknown)";
        device.setDevideId(deviceId);

        device.setType(Util.getDeviceTypeByName(selectDeviceTypeSP.getSelectedItem().toString()));

        device.setIpAddress(ipAddressTF.getText().toString());

        if(!portNF.getText().toString().isEmpty()) {
            device.setPort(portNF.getText().toString());
        } else {
            device.setPort(null);
        }

        device.setGetDataEndpoint(getEndpointTF.getText().toString());

        device.setPostDataEndpont(postEndpointTF.getText().toString());

        device.setAlarm(isAlarmCB.isChecked());

        RuntimeStorage.devicesMap.put(device.devideId, device);

        commitDeviceToDatabase(device);

        handleNavigateToHomeScreen(view);
    }

    public void removeDevice(View view) {
        handleNavigateToHomeScreen(view);
        DatabaseConnector.devicesCollectionHandler.document(this.device.getDevideId()).delete();
        RuntimeStorage.devicesMap.remove(this.device.getDevideId());
    }

    public void commitDeviceToDatabase(M_Device device) {
        DatabaseConnector.devicesCollectionHandler.document(device.getDevideId()).set(device);
    }

    public void loadFormData() {
        deviceIdTF.setText(device.getDevideId());
        deviceIdTF.setEnabled(false);

        deviceNameTF.setText(device.getName());
        ipAddressTF.setText(device.getIpAddress());
        portNF.setText(String.valueOf(device.getPort()));
        getEndpointTF.setText(device.getGetDataEndpoint());
        postEndpointTF.setText(device.getPostDataEndpont());
        selectDeviceTypeSP.setSelection(device.getType().ordinal());
        isAlarmCB.setChecked(device.getAlarm());


    }

    public void handleNavigateToHomeScreen(View view) {
        Intent intent = new Intent(L_AddNewDevice.this, L_HomeScreen.class);
        startActivity(intent);
    }

    public void setSpinnerValues() {
        Spinner spinner = (Spinner) findViewById(R.id.selectDeviceTypeSP);

        // TODO Move that section to Util Class
        List<String> list = new ArrayList<String>();
            list.add("MOVEMENT_SENSOR");
            list.add("SWITCH");
            list.add("TEMPERATURE_SENSOR");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

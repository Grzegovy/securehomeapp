package com.example.securehome;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class L_AddNewRoom extends Activity {

    String roomName;
    Room room;
    ContactsListAdapter adapter = null;

    EditText roomNameET;
    EditText roomWidthET;
    EditText roomheightET;

    ListView contactsListLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_room);

        initHandlers();

        Intent intent = getIntent();
        // Get room by name
        if(intent != null && intent.getStringExtra("roomName") != null) {
            roomName = intent.getStringExtra("roomName");
            room = RuntimeStorage.roomMap.get(roomName);
            loadRoomData();

            List<M_Contact> contactsList = new ArrayList<M_Contact>(room.getContactMap().values());
            contactsListLV = (ListView)findViewById(R.id.contactListLV);
            adapter = new ContactsListAdapter(this, contactsList, room.getName());
            adapter.bindAdapter(adapter);

            contactsListLV.setAdapter(adapter);
        } else {
            room = new Room();
            Button btn = (Button)findViewById(R.id.addContactBT);
            btn.setEnabled(false);
        }
    }

    public void initHandlers() {
        roomNameET = (EditText)findViewById(R.id.roomNameTF);
        roomWidthET = (EditText)findViewById(R.id.roomWidthNF);
        roomheightET = (EditText)findViewById(R.id.roomHeightNF);
    }

    public void loadRoomData() {
        if(room != null) {
            roomNameET.setEnabled(false);
            roomNameET.setText(room.getName());
            roomheightET.setText(String.valueOf(room.getSizeY()));
            roomWidthET.setText(String.valueOf(room.getSizeX()));
        }
    }

    public void saveNewRoom(View view) {
        Room newRoom = new Room();
        newRoom.setColor(Color.GRAY);

        if(this.room != null) newRoom = room;

        String roomName = roomNameET.getText().toString();
        if(roomName.isEmpty()) roomName = "(unnamed)";
        newRoom.setName(roomName);


        newRoom.setSizeX(Integer.valueOf(roomWidthET.getText().toString()));
        newRoom.setSizeY(Integer.valueOf(roomheightET.getText().toString()));

        newRoom.setPositionX(200);
        newRoom.setPositionY(300);

        RuntimeStorage.roomMap.put(newRoom.name, newRoom);
        handleNavigateToHomeScreen(view);
        commitRoomToDatabase(newRoom);
    }

    public void commitRoomToDatabase(Room room) {
        room.setDragListener(null);
        DatabaseConnector.roomsCollectionHandler.document(room.getName()).set(room);
    }

    public void handleNavigateToHomeScreen(View view) {
        Intent intent = new Intent(L_AddNewRoom.this, L_HomeScreen.class);
        startActivity(intent);
    }

    public void addContactHandler(View view) {
        final Room room = this.room;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Contact");
        // Set up the input
        final EditText inputName = new EditText(this);
        final EditText inputPhone = new EditText(this);
        final TextView labelName = new TextView(this);
            labelName.setText("Contact Name");
        final TextView labelPhone = new TextView(this);
            labelPhone.setText("Contact Phone");

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text

        inputName.setInputType(InputType.TYPE_CLASS_TEXT);
        inputPhone.setInputType(InputType.TYPE_CLASS_PHONE);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(50, 50, 50, 0);

        layout.setLayoutParams(params);

        layout.addView(labelName);
        layout.addView(inputName);
        layout.addView(labelPhone);
        layout.addView(inputPhone);

        layout.setPadding(50,0,50,0);

        builder.setView(layout);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String contactName = inputName.getText().toString();
                String contactPhone = inputPhone.getText().toString();

                M_Contact contact = new M_Contact(contactName, contactPhone);
                room.addContact(contact);

                L_AddNewRoom.super.invalidateOptionsMenu();

                Intent intent = getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}

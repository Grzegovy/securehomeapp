package com.example.securehome;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.securehome.DevicesSendRequest.DSR_MovementSensor;
import com.example.securehome.DevicesSendRequest.DSR_Switch;
import com.example.securehome.DevicesSendRequest.DSR_TemperatureSensor;

import java.util.ArrayList;
import java.util.List;

public class L_DisplayDevice extends Activity {

    String roomName;
    String deviceId;
    ListView eventsList;
    List<M_EventLog> events;

    TextView deviceNameLB;
    TextView deviceIdLB;
    TextView deviceTypeLB;
    TextView getEndpointLB;
    TextView postEndpointLB;
    TextView ipAddressLB;
    TextView portLB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_device);
        hideLoadingScreen();

        Intent intent = getIntent();
        // Get room by name
        if(intent != null) {
            roomName = intent.getStringExtra("roomName");
            deviceId = intent.getStringExtra("deviceId");
        }

        eventsList = (ListView)findViewById(R.id.deviceEventsLogList);

        System.out.println("DEVICE ID: " + deviceId + " " + roomName );

        initVariables();
        loadDeviceDetails();

        RuntimeStorage.getDeviceEvents(deviceId);

        if(RuntimeStorage.loadingData.isLoadingData()) {
            showLoadingScreen();
            RuntimeStorage.loadingData.setListener(new Loading.ChangeListener() {
                @Override
                public void onChange() {
                    if(!RuntimeStorage.loadingData.isLoadingData()) {
                        events = new ArrayList<M_EventLog>(RuntimeStorage.deviceEventList.values());
                        System.out.println("LOADED EVENTS::");
                        System.out.println(events);

                        initComponent();
                        hideLoadingScreen();
                    }
                }
            });
        }
    }

    public void initVariables() {
        deviceNameLB = (TextView) findViewById(R.id.deviceNameLB);
        deviceIdLB = (TextView) findViewById(R.id.deviceIdLB);
        deviceTypeLB = (TextView) findViewById(R.id.deviceTypeLB);
        getEndpointLB = (TextView) findViewById(R.id.getEndpointLB);
        postEndpointLB = (TextView) findViewById(R.id.postEndpointLB);
        ipAddressLB = (TextView) findViewById(R.id.ipAddressLB);
        portLB = (TextView) findViewById(R.id.portLB);
    }

    public void loadDeviceDetails() {
        M_Device device = RuntimeStorage.devicesMap.get(deviceId);
        deviceNameLB.setText(device.getName());
        deviceIdLB.setText(device.getDevideId());
        deviceTypeLB.setText(Util.getDeviceTypeByType(device.getType()));
        getEndpointLB.setText(device.getGetDataEndpoint());
        postEndpointLB.setText(device.getPostDataEndpont());
        ipAddressLB.setText(device.getIpAddress());
        portLB.setText(device.getPort());
    }

    public void initComponent() {
        EventsListAdapter adapter = new EventsListAdapter(this, events);
        eventsList.setAdapter(adapter);
    }

    public void sendData(View view) {
        Util.DeviceType type = RuntimeStorage.devicesMap.get(deviceId).getType();
        Intent intent;
        switch(type) {
            case SWITCH:
                intent = new Intent(L_DisplayDevice.this, DSR_Switch.class);
                break;
            case TEMPERATURE_SENSOR:
                intent = new Intent(L_DisplayDevice.this, DSR_TemperatureSensor.class);
                break;
            case MOVEMENT_SENSOR:
                intent = new Intent(L_DisplayDevice.this, DSR_MovementSensor.class);
                break;
            default:
                intent = new Intent(L_DisplayDevice.this, DSR_Switch.class);
                break;

        }
        intent.putExtra("deviceId", deviceId);
        intent.putExtra("roomName", roomName);
        startActivity(intent);
    }

    public void backToRoom(View view) {
        Intent intent = new Intent(L_DisplayDevice.this, L_RoomDetails.class);
        intent.putExtra("roomName", roomName);
        startActivity(intent);
    }

    public void handleAlarmOff(View view) {
        final View v = view;
        hideLoadingScreen();
        RuntimeStorage.getDeviceEvents(deviceId);
        if(RuntimeStorage.loadingData.isLoadingData()) {
            showLoadingScreen();
            RuntimeStorage.loadingData.setListener(new Loading.ChangeListener() {
                @Override
                public void onChange() {
                    if(!RuntimeStorage.loadingData.isLoadingData()) {
                        events = new ArrayList<M_EventLog>(RuntimeStorage.deviceEventList.values());
                        RuntimeStorage.devicesMap.get(deviceId).setStatus(Util.DeviceStatus.READY);
                        RuntimeStorage.roomMap.get(roomName).refreshRoomStatus();
                        // Deactivate all active device events
                        for(M_EventLog evt : RuntimeStorage.deviceEventList.values()) {
                            if(evt.getDeactivated() == false) {
                                evt.setDeactivated(true);
                                DatabaseConnector.eventsCollectionHandler.document(evt.getTimestamp().toString()).update("deactivated", evt.getDeactivated());
                            }
                        }


                        handleNavigateToHomeScreen(v);
                        hideLoadingScreen();
                    }
                }
            });
        }
    }

    public void removeDevice(View view) {
        RuntimeStorage.devicesMap.get(this.deviceId).setUsed(false);
        DatabaseConnector.devicesCollectionHandler.document(this.deviceId).update("used", false);

        Room room = RuntimeStorage.roomMap.get(this.roomName);
        System.out.println("IS EXIST TO DELETE???");
        if(room.getDevicesIds().contains(this.deviceId)) {
            System.out.println("IS EXIST TO JEEEES");

            room.getDevicesIds().remove(this.deviceId);
            DatabaseConnector.roomsCollectionHandler.document(this.roomName).update("devicesIds", room.getDevicesIds());
        }

        backToRoom(view);
    }
    public void handleNavigateToHomeScreen(View view) {
        Intent intent = new Intent(L_DisplayDevice.this, L_HomeScreen.class);
        startActivity(intent);
    }

    public void showLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    public void hideLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }
}

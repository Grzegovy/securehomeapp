package com.example.securehome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class L_DisplayDevices extends Activity {

    ListView devicesListView;
    String freeDevicesOnly;
    String roomName;
    List<M_Device> devicesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_devices);
        hideLoadingScreen();

        devicesList = new ArrayList<>();

        Intent intent = getIntent();

        if(intent != null) {
            freeDevicesOnly = intent.getStringExtra("selectRoomDevice");
            if (freeDevicesOnly != null) {
                roomName = intent.getStringExtra("roomName");
                RuntimeStorage.getUnusedDevices();
            } else {
                // Show all devices (home screen)
                System.out.println("LOADING ALL DEVICES...");
                devicesList = new ArrayList<M_Device>(RuntimeStorage.devicesMap.values());
                System.out.println(devicesList);
            }
        }

        if(RuntimeStorage.loadingData.isLoadingData()) {
            showLoadingScreen();
            RuntimeStorage.loadingData.setListener(new Loading.ChangeListener() {
                @Override
                public void onChange() {
                    if(!RuntimeStorage.loadingData.isLoadingData()) {
                        devicesList = new ArrayList<M_Device>(RuntimeStorage.unusedDevicesMap.values());
                        initComponents();
                        hideLoadingScreen();
                    }
                }
            });
        } else {
            initComponents();
        }
    }

    public void initComponents() {
        System.out.println("INIT ALL DEVICES...");
        System.out.println(devicesList);

        devicesListView = (ListView)findViewById(R.id.devicesListLV);
        DevicesListAdapter adapter = new DevicesListAdapter(this, devicesList);
        devicesListView.setAdapter(adapter);

        if(freeDevicesOnly == null) {
            devicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView deviceId = (TextView) view.findViewById(R.id.deviceIdLB);
                    Intent intent = new Intent(L_DisplayDevices.this, L_AddNewDevice.class);
                    intent.putExtra("deviceId", deviceId.getText());
                    startActivity(intent);
                }
            });
        } else {
            devicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView deviceId = (TextView) view.findViewById(R.id.deviceIdLB);
                    Intent intent = new Intent(L_DisplayDevices.this, L_RoomDetails.class);
                    intent.putExtra("selectedDevice", deviceId.getText());
                    intent.putExtra("roomName", roomName);
                    startActivity(intent);
                }
            });
        }
    }


    public void handleNavigateToHomeScreen(View view) {
        Intent intent = new Intent(L_DisplayDevices.this, L_HomeScreen.class);
        startActivity(intent);
    }

    public void showLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    public void hideLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }

}

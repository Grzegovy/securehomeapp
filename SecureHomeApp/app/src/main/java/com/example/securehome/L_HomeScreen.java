package com.example.securehome;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import static com.example.securehome.RuntimeStorage.CHANNEL_1_ID;
import static com.example.securehome.RuntimeStorage.CHANNEL_2_ID;

public class L_HomeScreen extends AppCompatActivity {
    public NotificationManagerCompat notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        notificationManager = NotificationManagerCompat.from(this);

        final L_HomeScreen hs = this;

        int hasSMSPermission = hs.checkSelfPermission(Manifest.permission.SEND_SMS);
        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            if (!hs.shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                Util.showMessageOKCancel("You need to allow access to Send SMS",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    hs.requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                                            Util.REQUEST_SMS);
                                }
                                hs.recreate();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hs.finishAffinity();
                            }
                        },
                        hs
                );
                return;
            }
            hs.requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                    Util.REQUEST_SMS);
            if(hs.checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                hs.finishAffinity();
            }
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        hideLoadingScreen();

        if(RuntimeStorage.roomMap.isEmpty()) new RuntimeStorage();
        RuntimeStorage.homeScreenHandler = this;

        if(RuntimeStorage.loadingData.isLoadingData()) {
            showLoadingScreen();
            RuntimeStorage.loadingData.setListener(new Loading.ChangeListener() {
                @Override
                public void onChange() {
                    if(!RuntimeStorage.loadingData.isLoadingData()) {
                        generateRoomPlan();
                        hideLoadingScreen();
                    }
                }
            });
        }

        Switch enableRoomsDragging = (Switch) findViewById(R.id.dragEnable);
        enableRoomsDragging.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RuntimeStorage.roomsDragEnable = isChecked;
                generateRoomPlan();
            }

        });

        generateRoomPlan();

        createNotificationChannels();
    }

    public void showLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    public void hideLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }

    public void handleNavigateToAddNewRoom(View view) {
        Intent intent = new Intent(L_HomeScreen.this, L_AddNewRoom.class);
        startActivity(intent);
    }

    public void handleNavigateToAddNewDevice(View view) {
        Intent intent = new Intent(L_HomeScreen.this, L_AddNewDevice.class);
        startActivity(intent);
    }

    public void handleNavigateToDisplayDevices(View view) {
        Intent intent = new Intent(L_HomeScreen.this, L_DisplayDevices.class);
        startActivity(intent);
    }


    public void generateRoomPlan() {
        RelativeLayout cl = (RelativeLayout) findViewById(R.id.roomsPlanLayout);
        cl.removeAllViews();
        for(Room r : RuntimeStorage.roomMap.values()) {
            r.initDragListener();
            final Button btn = new Button(this);
            btn.setText(r.getName());
            btn.setWidth(r.getSizeX()*50);
            btn.setHeight(r.getSizeY()*50);
            System.out.println("ROOM STATUS IS: " + r.getStatus());
            btn.setBackgroundColor(getResources().getColor(Util.getRoomColorByStatus(r.getStatus())));

            //RelativeLayout cl = (RelativeLayout) findViewById(R.id.roomsPlanLayout);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(r.getSizeX() * 50, r.getSizeY() * 50);
            layoutParams.setMargins(r.getPositionX(), r.getPositionY(), 0, 0);
            cl.addView(btn, layoutParams);

            if(!RuntimeStorage.roomsDragEnable) {
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    Intent intent = new Intent(L_HomeScreen.this, L_RoomDetails.class);
                    intent.putExtra("roomName", btn.getText());
                    startActivity(intent);
                    }
                });
            } else {
                btn.setOnTouchListener(r.getDragListener());
            }
        }
    }

    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "Channel 1",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This is Channel 1");

            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,
                    "Channel 2",
                    NotificationManager.IMPORTANCE_LOW
            );
            channel2.setDescription("This is Channel 2");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);
        }
    }
}

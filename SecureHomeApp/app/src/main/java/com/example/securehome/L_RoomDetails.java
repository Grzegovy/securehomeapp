package com.example.securehome;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class L_RoomDetails extends Activity {

    ListView roomDevicesListLV;
    Room room;
    M_Device device;

    TextView roomName;
    TextView status;
    TextView width;
    TextView length;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_details);
        initVariables();

        Intent intent = getIntent();

        // Get room by name
        room = RuntimeStorage.roomMap.get(intent.getStringExtra("roomName"));

        String selectedDevice = null;
        if(intent != null) {
            selectedDevice = intent.getStringExtra("selectedDevice");
            if (selectedDevice != null) {
                handleAddedDevice(selectedDevice);
            }
        }

        List<M_Device> devicesList = new ArrayList<M_Device>(RuntimeStorage.getRoomDevicesMap(room.getName()).values());

        roomDevicesListLV = (ListView)findViewById(R.id.roomDevicesListLV);
        DevicesListAdapter adapter = new DevicesListAdapter(this, devicesList);
        roomDevicesListLV.setAdapter(adapter);

        roomDevicesListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedDevice = ((TextView) view.findViewById(R.id.deviceIdLB)).getText().toString();

                Intent intent = new Intent(L_RoomDetails.this, L_DisplayDevice.class);
                intent.putExtra("roomName", room.getName());
                intent.putExtra("deviceId", selectedDevice);
                startActivity(intent);
            }
        });

        setRoomData();

    }

    public void handleAddedDevice(String deviceId) {
        System.out.println("Added device: " + deviceId);
        room.addDevice(deviceId);
    }

    public void initVariables() {
        roomName =  (TextView) findViewById(R.id.nameLB);
        status =  (TextView) findViewById(R.id.statusLB);
        width =  (TextView) findViewById(R.id.widthLB);
        length = (TextView) findViewById(R.id.lengthLB);
    }

    public void setRoomData() {
        roomName.setText(room.getName());
        status.setText(Util.getRoomStatusByStatus(room.getStatus()));
        width.setText(String.valueOf(room.getSizeX()));
        length.setText(String.valueOf(room.getSizeY()));
    }

    public void editRoom(View view) {
        Intent intent = new Intent(L_RoomDetails.this, L_AddNewRoom.class);
        intent.putExtra("roomName", room.getName());
        startActivity(intent);
    }

    public void backHandler(View view) {
        Intent intent = new Intent(L_RoomDetails.this, L_HomeScreen.class);
        startActivity(intent);
    }

    public void addDeviceHandler(View view) {
        Intent intent = new Intent(L_RoomDetails.this, L_DisplayDevices.class);
        intent.putExtra("selectRoomDevice", "true");
        intent.putExtra("roomName", room.getName());
        startActivity(intent);
    }

    public void showLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    public void hideLoadingScreen() {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }

    public void handleNavigateToHomeScreen(View view) {
        Intent intent = new Intent(L_RoomDetails.this, L_HomeScreen.class);
        startActivity(intent);
    }


}

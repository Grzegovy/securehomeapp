package com.example.securehome;

public class Loading {
    private boolean loadingData = false;
    private ChangeListener listener;

    public Loading(boolean loadingData) {
        this.loadingData = loadingData;
    }

    public boolean isLoadingData() {
        return loadingData;
    }

    public void setLoadingData(boolean loadingData) {
        this.loadingData = loadingData;
        if (listener != null) listener.onChange();
    }

    public ChangeListener getListener() {
        return listener;
    }

    public void setListener(ChangeListener listener) {
        this.listener = listener;
    }

    public interface ChangeListener {
        void onChange();
    }
}
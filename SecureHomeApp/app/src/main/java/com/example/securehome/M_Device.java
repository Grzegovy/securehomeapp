package com.example.securehome;

import android.os.AsyncTask;

import com.example.securehome.API.ResponseWrapper;
import com.example.securehome.API.SwitchRequestPOST;
import com.example.securehome.DevicesSendRequest.DSR_Switch;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class M_Device {
    String devideId;
    String name;
    Util.DeviceType type;
    Util.DeviceStatus status;
    String lastActivityDate;
    Map<String, M_EventLog> events;
    Boolean isAlarm;
    Long lasIpChange;

    String ipAddress;
    String port;
    String getDataEndpoint;
    String postDataEndpont;

    Boolean used;

    public M_Device() {
        this.used = false;
        this.status = Util.DeviceStatus.OFF;
        this.events = new HashMap<String, M_EventLog>();
        this.isAlarm = false;
        this.lasIpChange = Long.valueOf(0);
    }

    public void setStatus(Util.DeviceStatus status) {
        this.status = status;
        DatabaseConnector.devicesCollectionHandler.document(this.devideId).update("status", status);

        if(status == Util.DeviceStatus.ALARM) {
            // Set alarm on parent
            for(Room room : RuntimeStorage.roomMap.values()) {
                if(room.devicesIds.contains(this.devideId)) {
                    room.setStatus(Util.RoomStatus.ALARM);
                    return;
                }
            }
        }
    }

    public Long getLasIpChange() {
        return lasIpChange;
    }

    public void setLasIpChange(Long lasIpChange) {
        this.lasIpChange = lasIpChange;
    }

    public void enableAlarm() {
        if(this.isAlarm) changeAlarmStatus(Util.DeviceStatus.ENABLED);
    }

    public void disableAlarm() {
        if(this.isAlarm) changeAlarmStatus(Util.DeviceStatus.DISABLED);
    }

    public void changeAlarmStatus(Util.DeviceStatus status) {
        System.out.println("Setting device Alarm Status to: " + status);

        String endpoint = "http://" + this.getIpAddress();
        endpoint += (this.getPort() != null && !this.getPort().equals("-")) ? ":" + this.getPort() : "" ;
        endpoint += this.getPostDataEndpont();

        new AlarmStatus(status, endpoint).execute();
    }


    private class AlarmStatus extends AsyncTask<String, Void, String> {
        String requestEndpoint;
        Util.DeviceStatus status;

        public AlarmStatus(Util.DeviceStatus status, String requestEndpoint) {
            System.out.println("Creating alarm status!");
            this.requestEndpoint = requestEndpoint;
            this.status = status;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                System.out.println("Sending message...");
                SwitchRequestPOST srw = new SwitchRequestPOST();
                srw.setDeviceId(devideId);
                srw.setState((status == Util.DeviceStatus.ENABLED) ? 0 : 1);

                ObjectMapper Obj = new ObjectMapper();
                String requestBodyString = null;
                try {
                    requestBodyString = Obj.writeValueAsString(srw);
                    ResponseWrapper rw = DSR_Switch.sendPostRequest(requestEndpoint, requestBodyString);
                    if(rw.getStatusCode() == 200) {
                        System.out.println("Request processing complete!");
                        setStatus(status);
                    } else {
                        System.out.println("Request sending error!");
                        System.out.println(rw.getMessage());
                    }
                    System.out.println(rw.getMessage());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void addEvent(M_EventLog event) {
        events.put(event.getDateTime(), event);
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public String getDevideId() {
        return devideId;
    }

    public void setDevideId(String devideId) {
        this.devideId = devideId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Util.DeviceType getType() {
        return type;
    }

    public void setType(Util.DeviceType type) {
        this.type = type;
    }

    public Util.DeviceStatus getStatus() {
        return status;
    }

    public String getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(String lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    public String getGetDataEndpoint() {
        return getDataEndpoint;
    }

    public void setGetDataEndpoint(String getDataEndpoint) {
        this.getDataEndpoint = getDataEndpoint;
    }

    public String getPostDataEndpont() {
        return postDataEndpont;
    }

    public void setPostDataEndpont(String postDataEndpont) {
        this.postDataEndpont = postDataEndpont;
    }

    public Boolean getAlarm() {
        return isAlarm;
    }

    public void setAlarm(Boolean alarm) {
        isAlarm = alarm;
    }

    @Override
    public String toString() {
        return "M_Device{" +
                "devideId='" + devideId + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", status=" + status +
                ", lastActivityDate='" + lastActivityDate + '\'' +
                ", events=" + events +
                ", isAlarm=" + isAlarm +
                ", lasIpChange=" + lasIpChange +
                ", ipAddress='" + ipAddress + '\'' +
                ", port='" + port + '\'' +
                ", getDataEndpoint='" + getDataEndpoint + '\'' +
                ", postDataEndpont='" + postDataEndpont + '\'' +
                ", used=" + used +
                '}';
    }

    public Map<String, M_EventLog> getEvents() {
        return events;
    }

    public void setEvents(Map<String, M_EventLog> events) {
        this.events = events;
    }
}

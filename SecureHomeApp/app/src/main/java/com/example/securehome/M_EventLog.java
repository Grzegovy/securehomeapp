package com.example.securehome;

import java.util.HashMap;
import java.util.Map;

public class M_EventLog implements Comparable< M_EventLog > {
    String deviceId;
    String dateTime;
    Util.DeviceStatus status;
    Boolean deactivated;
    Long timestamp;
    String deviceIp;

    @Override
    public int compareTo(M_EventLog o) {
        return this.getDateTime().compareTo(o.getDateTime());
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;

        // Refresh ip in device
        M_Device device = RuntimeStorage.devicesMap.get(deviceId);
        if(device != null && deviceIp != null && device.getIpAddress() != deviceIp) {
            if(device.getLasIpChange() == null || timestamp > device.getLasIpChange()) {
                System.out.println("[Device: " + deviceId + "] Changing ip address to: " + deviceIp);
                device.setLasIpChange(timestamp);
                device.setIpAddress(deviceIp);

                Map<String, Object> dataToUpdate = new HashMap<>();
                dataToUpdate.put("ipAddress", deviceIp);
                dataToUpdate.put("lasIpChange", timestamp);
                DatabaseConnector.devicesCollectionHandler.document(device.devideId).update(dataToUpdate);
            }
        }
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Util.DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(Util.DeviceStatus status) {
        this.status = status;
    }

    public Boolean getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(Boolean deactivated) {
        this.deactivated = deactivated;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}


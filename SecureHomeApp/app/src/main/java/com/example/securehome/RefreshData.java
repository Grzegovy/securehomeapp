package com.example.securehome;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.Button;

import java.sql.Time;
import java.util.TimerTask;

public class RefreshData extends TimerTask {
    Button btn;

    public RefreshData(Button btn) {
        this.btn = btn;

    }

    public void run() {
        System.out.println("Timer data processing...!");
        // Get latest events marked as unread
        if(((ColorDrawable)(btn.getBackground())).getColor() == Color.RED) {
            btn.setBackgroundColor(Color.GRAY);
            System.out.println("SET GRAY");
        } else {
            btn.setBackgroundColor(Color.RED);
            System.out.println("SET RED " + btn.getSolidColor());
        }
        // Process data


        // Mark events as processed
    }
}
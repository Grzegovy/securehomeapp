package com.example.securehome;

import android.app.Notification;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.util.ArrayMap;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.securehome.RuntimeStorage.CHANNEL_1_ID;

public class Room {
    String name;
    List<String> devicesIds;
    Map<String, M_Contact> contactMap;
    Util.RoomStatus status;

    private int sizeX;
    private int sizeY;
    private int positionX;
    private int positionY;
    Integer color;

    private int xDelta;
    private int yDelta;

    Room.dragRoomEventListener dragListener;

    public Room() {
        devicesIds = new ArrayList<String>();
        contactMap = new HashMap<String, M_Contact>();
        status = Util.RoomStatus.EMPTY;
    }

    public void addDevice(String deviceId) {
        devicesIds.add(deviceId);
        refreshRoomStatus();
        //RuntimeStorage.refreshStorage();
    }

    public void updateRoomDevicesListInDB() {
        Map<String, Object> dataToUpdate = new HashMap<>();
        dataToUpdate.put("devicesIds", devicesIds);
        dataToUpdate.put("status", status);
        if(this.name != null) DatabaseConnector.roomsCollectionHandler.document(this.name).update(dataToUpdate);

        for(String device : devicesIds) {
            DatabaseConnector.devicesCollectionHandler.document(device).update("used", true);
        }
    }

    public void addContact(M_Contact contact) {
        if(contactMap == null) contactMap = new HashMap<>();
        this.contactMap.put(contact.getName(), contact);
        refreshContactsInDatabase();
    }

    public void removeContact(String contactName) {
        this.contactMap.remove(contactName);
        refreshContactsInDatabase();
    }

    public void refreshContactsInDatabase() {
        Map<String, Object> dataToUpdate = new HashMap<>();
        dataToUpdate.put("contactMap", contactMap);
        DatabaseConnector.roomsCollectionHandler.document(this.name).update(dataToUpdate);
    }

    public void refreshRoomStatus() {
        if(devicesIds.isEmpty()) {
            this.status = Util.RoomStatus.EMPTY;
            updateRoomDevicesListInDB();
            return;
        }

        ArrayList<M_Device> devicesList = new ArrayList();
        for(String deviceId : devicesIds) {
            devicesList.add(RuntimeStorage.devicesMap.get(deviceId));
        }

        for(M_Device device : devicesList) {
            if(device.getStatus() == Util.DeviceStatus.ALARM) {
                this.status = Util.RoomStatus.ALARM;
                updateRoomDevicesListInDB();
                enableAlarmDevices();
                return;
            }
            if(device.getStatus() == Util.DeviceStatus.ERROR) {
                this.status = Util.RoomStatus.DEVICE_ERROR;
                updateRoomDevicesListInDB();
                return;
            }
        }

        this.status = Util.RoomStatus.OK;
        updateRoomDevicesListInDB();
        disableAlarmDevices();
    }

    public void enableAlarmDevices() {
        for(String deviceId : devicesIds ) {
            M_Device dvc = RuntimeStorage.devicesMap.get(deviceId);
            if(dvc != null && dvc.getAlarm()) dvc.enableAlarm();
        }
    }

    public void disableAlarmDevices() {
        for(String deviceId : devicesIds ) {
            M_Device dvc = RuntimeStorage.devicesMap.get(deviceId);
            if(dvc != null && dvc.getAlarm()) dvc.disableAlarm();
        }
    }

    public void setStatus(Util.RoomStatus status) {
        if(status == Util.RoomStatus.ALARM && this.status != Util.RoomStatus.ALARM) {
            enableAlarmDevices();

            System.out.println("ALARM DETECTED FOR ROOM: " + this.name);
            Util.createNotification("Secure Home - Alarm Detected","Room: " + this.name + " changed it's state to ALARM. Action is Required!", RuntimeStorage.CHANNEL_1_ID);
            for(M_Contact cnt : this.contactMap.values()) {
                Util.sendSMS("Secure Home - Alarm Detected. Dear " + cnt.getName() + ", Room: " + this.name + " changed it's state to ALARM. Action is required!", cnt.getNumber());
            }
            this.status = status;
            updateRoomDevicesListInDB();
            RuntimeStorage.homeScreenHandler.recreate();
        } else {
            this.status = status;
            updateRoomDevicesListInDB();
        }
    }

    public void initDragListener() {
        this.dragListener = new dragRoomEventListener();
    }

    public class dragRoomEventListener implements View.OnTouchListener {

        // This is the method that the system calls when it dispatches a drag event to the
        // listener.
        public boolean onTouch(View v, MotionEvent event) {

            // Defines a variable to store the action type for the incoming event
            final int action = event.getAction();
            final int X = (int)event.getRawX();
            final int Y = (int)event.getRawY();

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();

            // Handles each of the expected events
            switch(event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    xDelta = snap(X - lp.leftMargin);
                    yDelta = snap(Y - lp.topMargin);
                    break;

                case MotionEvent.ACTION_MOVE:
                    lp.leftMargin = snap(X - xDelta);
                    lp.topMargin = snap(Y - yDelta);
                    lp.rightMargin = -250;
                    lp.bottomMargin = -250;
                    setPositionX(lp.leftMargin);
                    setPositionY(lp.topMargin);
                    break;
                case MotionEvent.ACTION_UP:
                    Map<String, Object> dataToUpdate = new HashMap<>();
                    dataToUpdate.put("positionX", positionX);
                    dataToUpdate.put("positionY", positionY);
                    DatabaseConnector.roomsCollectionHandler.document(name).update(dataToUpdate);
                    break;
                default:
                    Log.e("DragDrop Example","Unknown action type received by OnTouchListener." + event.getAction());
                    break;
            }

            v.setLayoutParams(lp);
            return false;
        }
    };

    public static int snap(Integer val) {
        return (int) Math.floor(val / 50) * 50;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getDevicesIds() {
        return devicesIds;
    }

    public void setDevicesIds(List<String> devicesIds) {
        this.devicesIds = devicesIds;
    }

    public Util.RoomStatus getStatus() {
        return status;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public int getxDelta() {
        return xDelta;
    }

    public void setxDelta(int xDelta) {
        this.xDelta = xDelta;
    }

    public int getyDelta() {
        return yDelta;
    }

    public void setyDelta(int yDelta) {
        this.yDelta = yDelta;
    }

    public dragRoomEventListener getDragListener() {
        return dragListener;
    }

    public void setDragListener(dragRoomEventListener dragListener) {
        this.dragListener = dragListener;
    }

    public Map<String, M_Contact> getContactMap() {
        return contactMap;
    }

    public void setContactMap(Map<String, M_Contact> contactMap) {
        this.contactMap = contactMap;
    }

    @Override
    public String toString() {
        return "Room{" +
                "name='" + name + '\'' +
                ", devicesIds=" + devicesIds +
                //", contactMap=" + contactMap +
                ", status=" + status +
                ", sizeX=" + sizeX +
                ", sizeY=" + sizeY +
                ", positionX=" + positionX +
                ", positionY=" + positionY +
                ", color=" + color +
                ", xDelta=" + xDelta +
                ", yDelta=" + yDelta +
                ", dragListener=" + dragListener +
                '}';
    }
}

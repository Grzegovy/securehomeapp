package com.example.securehome;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.provider.ContactsContract;

import com.example.securehome.DevicesDataTypes.DT_MovementSensorData;
import com.example.securehome.DevicesDataTypes.DT_TemperatureSensor;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuntimeStorage {
    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference eventsReference;

    // Room Definitions
    public static Map<String, Room> roomMap = new HashMap<String, Room>();
    public static Map<String, M_Device> devicesMap = new HashMap<String, M_Device>();
    public static Map<String, M_Device> unusedDevicesMap = new HashMap<String, M_Device>();
    public static Map<String, M_EventLog> deviceEventList = new HashMap<>();


    // Application Settings
    public static Boolean roomsDragEnable = false;
    public static Loading loadingData = new Loading(false);
    public static L_HomeScreen homeScreenHandler;

    public static final String CHANNEL_1_ID = "channel1";
    public static final String CHANNEL_2_ID = "channel2";


    public RuntimeStorage() {
        roomMap = new HashMap<String, Room>();
        devicesMap = new HashMap<String, M_Device>();

        firebaseDatabase = FirebaseDatabase.getInstance();
        eventsReference = firebaseDatabase.getReference("events");

        roomMap = DataLoader.getRooms();
        devicesMap = DataLoader.getDevices();
        //insertSampleLogs();
    }

    public static void refreshStorage() {
        roomMap = DataLoader.getRooms();
        devicesMap = DataLoader.getDevices();
    }

    public static void getDeviceEvents(String deviceId) {
        deviceEventList = DataLoader.getDeviceEvents(deviceId);
    }

    public static void getUnusedDevices() {
        unusedDevicesMap = DataLoader.getUnusedDevices();
    }

    public static Map<String, M_Device> getRoomDevicesMap(String roomName) {
        List<String> roomsDevicesIds = roomMap.get(roomName).getDevicesIds();

        Map<String, M_Device> devices = new HashMap<>();

        for(String deviceId : roomsDevicesIds) {
            devices.put(RuntimeStorage.devicesMap.get(deviceId).getDevideId(), RuntimeStorage.devicesMap.get(deviceId));
        }

        return  devices;
    }

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public void setFirebaseDatabase(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference getEventsReference() {
        return eventsReference;
    }

    public void setEventsReference(DatabaseReference eventsReference) {
        this.eventsReference = eventsReference;
    }

    public void insertSampleLogs() {

        // Create logs
        DT_MovementSensorData msd = new DT_MovementSensorData();
        msd.setMovementDetected("true");
        msd.setDeviceId("0000000001");
        msd.setDateTime("2018-10-11 12:33:21");
        msd.setDeactivated(false);
        msd.setStatus(Util.DeviceStatus.ALARM);

        // Create logs
        DT_MovementSensorData msd2 = new DT_MovementSensorData();
        msd2.setMovementDetected("true");
        msd2.setDeviceId("0000000001");
        msd2.setDateTime("2018-10-11 18:33:21");
        msd2.setDeactivated(false);
        msd2.setStatus(Util.DeviceStatus.ON);

        DatabaseConnector.eventsCollectionHandler.document(msd.getDateTime()).set(msd);
        DatabaseConnector.eventsCollectionHandler.document(msd2.getDateTime()).set(msd2);

    }

}


package com.example.securehome;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;

import java.util.List;

public class Util {

    public static final int REQUEST_SMS = 0;

    public enum DeviceType {
        MOVEMENT_SENSOR, SWITCH, TEMPERATURE_SENSOR, UNKNOWN
    }

    public static DeviceType getDeviceTypeByName(String name) {
        switch(name) {
            case "MOVEMENT_SENSOR": {
                return DeviceType.MOVEMENT_SENSOR;
            }
            case "SWITCH": {
                return DeviceType.SWITCH;
            }
            case "TEMPERATURE_SENSOR": {
                return DeviceType.TEMPERATURE_SENSOR;
            }
            default: {
                System.out.println("Unknown sensor!");
                return null;

            }
        }
    }

    public enum DeviceStatus {
        OFF, ON, ERROR, READY, ALARM, ENABLED, DISABLED, OK
    }

    public enum RoomStatus {
        OK, ALARM, DEVICE_ERROR, EMPTY
    }

    public static String getDeviceTypeByType(DeviceType type) {
        switch(type) {
            case MOVEMENT_SENSOR: return "MOVEMENT_SENSOR";
            case SWITCH: return "SWITCH";
            case TEMPERATURE_SENSOR: return "TEMPERATURE_SENSOR";
            case UNKNOWN: return "UNKNOWN";
            default: return "Other";
        }
    }

    public static String getRoomStatusByStatus(RoomStatus status) {
        switch(status) {
            case OK: return "Ok";
            case ALARM: return "ALARM";
            case DEVICE_ERROR: return "Device Error";
            case EMPTY: return "No devices";
            default: return "Unknown";
        }
    }

    public static int getRoomColorByStatus(RoomStatus status) {
        switch(status) {
            case OK: return android.R.color.background_light;
            //ase ALARM: return Color.RED; //android.R.color.holo_red_light;
            case ALARM: return android.R.color.holo_red_light;
            case DEVICE_ERROR: return android.R.color.holo_red_light;
            case EMPTY: return android.R.color.background_light;
            default: return android.R.color.holo_red_dark;
        }
    }

    public static String getStatusNameByStatus(DeviceStatus status) {
        switch(status) {
            case ON: return "Active";
            case OFF: return "Off";
            case ERROR: return "Error";
            case READY: return "Ready";
            case ALARM: return "ALARM";
            case ENABLED: return "Enabled";
            case DISABLED: return "Disabled";
            default: return "Unknown";
        }
    }

    public static void deactivateAllActiveDeviceEvents(String deviceId) {
        // Get device events

    }

    public static void createNotification(String title, String message, String channel) {
        Intent intent = new Intent(RuntimeStorage.homeScreenHandler, L_HomeScreen.class);
        PendingIntent pi = PendingIntent.getActivity(RuntimeStorage.homeScreenHandler, 1, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification = new NotificationCompat.Builder(RuntimeStorage.homeScreenHandler, channel)
                .setSmallIcon(R.drawable.common_google_signin_btn_text_dark_normal)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_EVENT)
                .setAutoCancel(true)
                .setContentIntent(pi)
                .setSound(soundUri)
                .build();


        RuntimeStorage.homeScreenHandler.notificationManager.notify(1, notification);
    }

    public static void sendSMS(String message, String phone) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            int hasSMSPermission = RuntimeStorage.homeScreenHandler.checkSelfPermission(Manifest.permission.SEND_SMS);
            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                if (!RuntimeStorage.homeScreenHandler.shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                    showMessageOKCancel(
                            "You need to allow access to Send SMS",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        RuntimeStorage.homeScreenHandler.requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                                                REQUEST_SMS);
                                    }
                                }
                            },
                            null,
                            RuntimeStorage.homeScreenHandler
                    );
                    return;
                }
                RuntimeStorage.homeScreenHandler.requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                        REQUEST_SMS);
                return;
            }
            SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (String msg : messages) {
                PendingIntent sentIntent = PendingIntent.getBroadcast(RuntimeStorage.homeScreenHandler, 0, new Intent("SMS_SENT"), 0);
                PendingIntent deliveredIntent = PendingIntent.getBroadcast(RuntimeStorage.homeScreenHandler, 0, new Intent("SMS_DELIVERED"), 0);
                sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

            }
        }
    }

    public static void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener, AppCompatActivity activity) {
        new android.support.v7.app.AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .create()
                .show();
    }
}

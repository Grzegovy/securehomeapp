const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const express = require('express');

firebase.initializeApp({
    apiKey: "AIzaSyAE2haf9T7l3wTXAPcsyaXyDB4WJlq7q80",
    authDomain: "wpam-grswi.firebaseapp.com",
    databaseURL: "https://wpam-grswi.firebaseio.com",
    projectId: "wpam-grswi",
    storageBucket: "wpam-grswi.appspot.com",
    messagingSenderId: "97696400848"
});


const app = express();

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }

app.post('/events', (request, response) => {
    console.log(request);

    var updates = {};
    var dataType = request.get('DeviceType');

    var resultData = new Object();

    // Get info from device
    console.log(request.body);
    console.log(request.body.deviceId);
    var deviceData = request.body;
    var dataKeys = Object.keys(deviceData);
    dataKeys.forEach(function(key) {
        resultData[key] = deviceData[key];
    });

    // Add custom parameters
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1).pad(2) + "-" + current_datetime.getDate().pad(2) + " " + current_datetime.getHours().pad(2) + ":" + current_datetime.getMinutes().pad(2) + ":" + current_datetime.getSeconds().pad(2);

    var date = new Date();
    var timestampString = date.getTime().toString();
    console.log("Document timestamp: " + timestampString);

    resultData["dateTime"] = formatted_date;
    resultData["deactivated"] = false;
    resultData["timestamp"] = date.getTime()

    //updates['/events/' + d.getTime()] = resultData;

    console.log(resultData);
    console.log("Before sending...");

    var resp = "OK"
    var status = 200;

    const db = firebase.firestore();
    db.collection("events").doc(timestampString).set(resultData)
        .then(function() {
            console.log("Document successfully written!");
        })
        .catch(function(error) {
            resp = error;
            status = 500;
            console.log("Error writing document: ", error);
        });

    //firebase.database().ref().update(updates);

    response.send(resp);
    response.status(status);
})

app.get('/home', (request, response) => {
    response.send('Hello!');
    response.status(200);
});

app.get('/devices', (request, response) => {

    var resp = ":)";
    console.log("GRSWI");
    console.log(firebase.database().ref('/devices').once('value'));
    console.log(firebase.database().ref('devices').once('value'));
    return firebase.database().ref('/devices').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      console.log(snapshot);
      console.log(snapshot.val());
      //response.send(snapshot);
      response.send(snapshot.val());
      response.status(200);
    });

});

app.post('/addDevice', (request, response) => {
    console.log(request);

    var newPostKey = firebase.database().ref().child('/addDevice').push().key;

    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    var postData = {
        deviceType: request.get('DeviceType'),
        deviceId: request.get('DeviceId'),
        eventDate: Date.now(),
        data: request.body
      };
    updates['/events/' + newPostKey] = postData;
  

    firebase.database().ref().update(updates);

    response.send('OK!');
    response.status(200);
})

exports.app = functions.https.onRequest(app);
exports.firebase = functions.https.onRequest(firebase);


// var admin = require('firebase-admin');


// var config = {
//     apiKey: "AIzaSyAE2haf9T7l3wTXAPcsyaXyDB4WJlq7q80",
//     authDomain: "wpam-grswi.firebaseapp.com",
//     databaseURL: "https://wpam-grswi.firebaseio.com",
//     projectId: "wpam-grswi",
//     storageBucket: "wpam-grswi.appspot.com",
//     messagingSenderId: "97696400848"
//   };
//   admin.initializeApp(config);

// // admin.initializeApp({
// //   credential: admin.credential.applicationDefault(),
// //   databaseURL: 'https://wpam-grswi.firebaseio.com'
// // });

// var defaultAuth = admin.auth();
// var defaultDatabase = admin.database();

// // The app only has access as defined in the Security Rules
// var db = admin.database();
// var ref = db.ref("/devices");
// ref.once("value", function(snapshot) {
//   console.log(snapshot.val());
// });